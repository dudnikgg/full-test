var app = angular.module('full-test', ['ngSanitize']);

app.controller('testController', testController);

function testController($scope, $http) {
  if(api_post_option.isChecked === 'on') {
    $scope.posts = [];
    $http.get("http://demo.wp-api.org/wp-json/wp/v2/posts?per_page=5")
    .then(function(data) {
      angular.forEach(data.data, function(val, index) {
        $scope.posts.push(val);
        $scope.getCatLink = val._links["wp:term"][0].href;
        $scope.getTagsLink = val._links["wp:term"][1].href;

        // Getting categories for current post
        $http.get($scope.getCatLink).then(function(catData) {
          $scope.catName = [];
          $scope.catLink = [];
          angular.forEach(catData.data, function(val) {
            $scope.catLink.push(val.link);
            $scope.catName.push(val.name);
            $scope.newCatArray = $scope.catName.map(function(value, i) {
              return {
                  name: value,
                  link:  $scope.catLink[i]
              }
            });
            $scope.posts[index].categories = $scope.newCatArray;
          });
        });

        // Getting tags for current post
        $http.get($scope.getTagsLink).then(function(tagData) {
          $scope.tagName = [];
          $scope.tagLink = [];
          angular.forEach(tagData.data, function(val) {
            $scope.tagLink.push(val.link);
            $scope.tagName.push(val.name);
            $scope.newTagArray = $scope.tagName.map(function(value, i) {
              return {
                  name: value,
                  link:  $scope.tagLink[i]
              }
            });
            $scope.posts[index].tags = $scope.newTagArray;
          });
        });
      });
    }, function(error) {
      console.log(error)
    })
  } else {
    alert('API is OFF!');
  }
}