### Put this files in twentysixteen-child folder 
Test tasks:

* 1) Create a child theme of twentysixteen.
* 2) Replace main posts feed on start page by fetching posts from the WP REST API (v2).
* 3) Add control in WordPress admin for enable/disable printing of your new post feed.
* 4) Add indicator in each post on start page feed that tells the visitor if the post that is being displayed is fetched from the API.

Completed tasks:

* 1) DONE
* 2) DONE, created template, file - postsFromAPI.php
* 3) DONE, created button in WP admin menu and checkbox to on/off api posts.
* 4) DONE

Live demo here - http://full-test.lovelike.in.ua/

### Admin panel access
admin link - http://full-test.lovelike.in.ua/wp-admin

Login - fulltest
Password - qwerty