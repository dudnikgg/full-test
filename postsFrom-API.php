<?php
/**
 * Template Name: Posts from WP REST API
 */
?>
<?php get_header(); ?>

<app ng-app="full-test">
  <div ng-controller="testController">
    <article ng-repeat="post in posts" class="post">
      <header class="entry-header">
        <h2 class="entry-title">
          <a href="{{ post.link }}" rel="bookmark">{{ post.title.rendered }}</a>
        </h2>
      </header>

      <div ng-bind-html="post.content.rendered" class="entry-content">{{ post.content.rendered }}</div>

      <footer class="entry-footer">
        <span class="posted-on">
          <span class="screen-reader-text">Posted on </span>
          <a href="https://wp-themes.com/?p=19" rel="bookmark">
            <time class="entry-date published" datetime="{{ post.date }}">{{ post.date | date: 'MMMM d, yyyy' }}</time>
            <time class="updated" datetime="{{ post.modified }}">{{ post.date | date: 'MMMM d, yyyy' }}</time>
          </a>
        </span>
        <span class="cat-links">
          <span class="screen-reader-text">Categories </span>
          <a href="{{ cat.link }}" ng-repeat="cat in post.categories"> {{ cat.name }}{{$last ? '' : ', '}}</a>
        </span>
        <span class="tags-links">
          <span class="screen-reader-text">Tags </span>
          <a href="{{ tag.link }}" ng-repeat="tag in post.tags">{{ tag.name }}{{$last ? '' : ', '}} </a>
        </span>
        <span class="api-indicator">This post from API</span>
      </footer>
    </article>
  </div>
</app>

