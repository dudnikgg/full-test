<?php

  function full_test_styles() {

    $parent_style = 'twentysixteen-style';

    wp_enqueue_style($parent_style, get_template_directory_uri() . '/style.css');
    wp_enqueue_style('full-test-style', get_stylesheet_directory_uri() . '/style.css', array( $parent_style ), wp_get_theme()->get('Version'));

  }

  function full_test_scripts() {
    wp_enqueue_script('angularjs', 'https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.4/angular.js');
    wp_enqueue_script('ngSanitize', 'https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.1/angular-sanitize.min.js');
    wp_enqueue_script('full-test-scripts', get_stylesheet_directory_uri() . '/main.js', array( 'jquery' ));
    wp_localize_script( 'full-test-scripts', 'api_post_option', array('isChecked' => get_option('toggle_posts_from_api', '')) );
  }

  function child_styles() {
    echo '<link rel="stylesheet" href="'.get_stylesheet_directory_uri().'/style.css" type="text/css" media="all" />';
  }

  add_action('admin_head', 'child_styles');

  add_action( 'wp_enqueue_scripts', 'full_test_styles' );
  add_action( 'wp_enqueue_scripts', 'full_test_scripts' );

  // Creating enable/disable API posts page and saving option //
  if ( is_admin() ) {
    add_action( 'admin_menu', 'add_menu_toggle_api' );
  }

  function add_menu_toggle_api() {
    add_menu_page( 'Toggle API posts', 'Toggle API posts', 'manage_options', 'toggle-api-posts', 'toggle_api_posts_page');
    add_action( 'admin_init', 'register_menu_toggle_api' );
  }

  if( !function_exists("register_menu_toggle_api") ) {
    function register_menu_toggle_api() {
      register_setting( 'toggle-posts-from-api-settings', 'toggle_posts_from_api' );
    }
  }

  function toggle_api_posts_page() {
    if (isset($_POST['toggle_posts_from_api'])) {
      update_option('toggle_posts_from_api', $_POST['toggle_posts_from_api']);
      $is_checked = $_POST['toggle_posts_from_api'];
    }

    $is_checked = get_option('toggle_posts_from_api', 'off');

?>
  <h1>Enable or Disable posts from API</h1>

  <form method="post" action="options.php">
    <?php settings_fields( 'toggle-posts-from-api-settings' ); ?>
    <?php do_settings_sections( 'toggle-posts-from-api-settings' ); ?>

    <div class="api-button">
      <span>Disable</span>
        <label class="switch">
          <input type="checkbox" name="toggle_posts_from_api" <?php echo $is_checked ? 'checked' : ''; ?>>
          <div class="slider"></div>
        </label>
      <span>Enable</span>
    </div>
    <?php submit_button(); ?>
  </form>
<?php } ?>
